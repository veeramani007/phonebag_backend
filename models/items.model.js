var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var itemSchema = new Schema({

    img: {
        type: String
    },
    Subimg1: {
        type: String
    },
    Subimg2: {
        type: String
    },
    brand: {
        type: String
    },
    name: {
        type: String
    },
    category: {
        type: String
    },
    desc: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
},
    { collection: 'items' });


module.exports = mongoose.model('itemDetails', itemSchema);