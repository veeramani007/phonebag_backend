var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cartSchema = new Schema({
    img: {
        type: String
    },
    brand: {
        type: String
    },
    name: {
        type: String
    },
    price: {
        type: Number
    },
    qty: {
        type: Number
    },
},
    { collection: 'cart' });

module.exports = mongoose.model("cartData", cartSchema);