var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Item = mongoose.model('itemDetails');

//response checking

router.get('/res', function (req, res, next) {
    res.send('yes i am working');
});

//Add items
router.post('/add_Item', (req, res) => {


    console.log("done", req.body)

    var img = req.body.img;
    var Subimg1 = req.body.Subimg1;
    var Subimg2 = req.body.Subimg2;
    var brand = req.body.brand;
    var name = req.body.name;
    var category = req.body.category;
    var desc = req.body.desc;
    var price = req.body.price;

    const itemData = { img, Subimg1, Subimg2, brand, name, category, desc, price };
    var ItemModel = new Item(itemData);

    ItemModel.save(itemData, function (err, itemData) {
        if (err) {
            console.log(err);
            throw err;
        }
        else {
            console.log("Result -> " + itemData);
            res.json(itemData);

        }
    });
});

//get All data
router.get('/all_Items', (req, res) => {
    Item.find({}, (err, data) => {
        if (err)
            res.send(err);
        else
            res.json(data);
    });
});

//Get single Item
router.get('/one_Item/:id', (req, res) => {

    Item.findOne({ _id: req.params.id }, (err, item) => {
        if (err)
            res.send(err);
        else
            res.json(item);
    });
});

//Edit Item
router.put('/edit_Item/:id', (req, res) => {
    const update = {
        $set: {
            name: req.body.name,
            category: req.body.category,
            qty: req.body.qty,
            price: req.body.price
        }
    };

    Item.findByIdAndUpdate({ _id: req.params.id }, update, (err, dataUp) => {
        if (err) throw err;
        else
            res.json(update);
    });
});

//Delete By id
router.delete('/delete_Item/:id', (req, res) => {
    var id = req.params.id;
    Item.findById(id, (err, data) => {
        if (err)
            res.send(err);
        else {
            data.remove((error, dlted) => {
                if (err)
                    res.send(error);
                else {
                    res.json(dlted);
                    console.log('deleted Succesfull');
                }
            });
        }
    });
});

module.exports = router;