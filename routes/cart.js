var express = require('express');
var router = express.Router();
var mongoos = require('mongoose');
var Data = mongoos.model('cartData');

router.get('/res', function (req, res, next) {
    res.send('yes i am working');
});

//SAVE ITEM TO CART

router.post('/itemAdd', (req, res) => {

    console.log("done", req.body);

    var img = req.body.img;
    var brand = req.body.brand;
    var name = req.body.name;
    var price = req.body.price;
    var qty = req.body.qty;

    const itemDetails = { img, brand, name, price, qty };
    var product = new Data(itemDetails);

    product.save(itemDetails, function (err, itemDetails) {

        if (err)
            res.send(err);
        else {
            console.log('result', itemDetails);

            res.json(itemDetails);
        }
    });
});

//GET all Data from Cart
router.get('/showcart', (req, res) => {
    Data.find({}, (err, data) => {
        if (err)
            res.send(err);
        else
            res.json(data);
    });
});

router.delete('/dltAll', (req, res) => {
    Data.remove({}, (err, data) => {
        if (err)
            res.send(err);
        else
            res.json(data);
    });
});

//GET SINLGE DATA
router.get('/one_Item/:id', (req, res) => {

    Data.findOne({ _id: req.params.id }, (err, item) => {
        if (err)
            res.send(err);
        else
            res.json(item);
    });
});

//Edit Item
router.put('/edit_Item/:id', (req, res) => {
    const update = {
        $set: {
            qty: req.body.qty
        }
    };

    Data.findByIdAndUpdate({ _id: req.params.id }, update, (err, dataUp) => {
        if (err) throw err;
        else {
            res.json(update);
            console.log("updated", update);

        }
    });
});

//DELETE FROM CART
router.delete('/remove_item/:id', (req, res) => {
    var id = req.params.id;
    Data.findById(id, (err, data) => {
        if (err)
            res.send(err);
        else {
            data.remove((error, dlted) => {
                if (err)
                    res.send(error);
                else {
                    res.json(dlted);
                    console.log('deleted Succesfull' + dlted);
                }
            });
        }
    });
});

module.exports = router;